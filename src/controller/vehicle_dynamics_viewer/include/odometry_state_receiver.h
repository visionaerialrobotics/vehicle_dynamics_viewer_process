/*!*******************************************************************************************
 *  \file       odometry_state_receiver.h
 *  \brief      OdometryStateReceiver definition file.
 *  \details    This file includes the OdometryStateReceiver class declaration. To obtain more
 *              information about it's definition consult the odometry_state_receiver.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef VehicleDynamics_ODOMETRYSTATERECEIVER_H_
#define VehicleDynamics_ODOMETRYSTATERECEIVER_H_

#include <ros/ros.h>
#include <ros/network.h>

#include "std_msgs/String.h"
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>

#include <QString>
#include <QThread>
#include <QtDebug>
#include <QStringListModel>

#include <sstream>

/*****************************************************************************
** Class
*****************************************************************************/

class OdometryStateReceiver : public QObject
{
  Q_OBJECT
public:
  OdometryStateReceiver();
  virtual ~OdometryStateReceiver();
  bool ready();
  std::string drone_id;
  std::string drone_id_namespace;
  geometry_msgs::PoseStamped odometry_new_pose;
  void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);

Q_SIGNALS:
  void updateStatus();

private:
  ros::Subscriber geometric_pose_sub;
  std::string odometry_estimated_pose_str;
  std::string odometry_namespace;
  bool subscriptions_complete;
  int init_argc;
  int real_time;
  char** init_argv;

  void dronePoseCallback(const geometry_msgs::PoseStamped msg);
};
#endif
