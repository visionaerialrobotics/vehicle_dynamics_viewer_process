/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/vehicle_dynamics_viewer_view.h"

VehicleDynamicsViewerView::VehicleDynamicsViewerView(int argc, char** argv, QWidget* parent)
  : QWidget(parent, Qt::Window), ui(new Ui::VehicleDynamicsViewerView)
{
  ui->setupUi(this);

  // window always on top
  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);

  // Add the vehicle dynamics widget.
  vehicleDynamics = new VehicleDynamicsViewer(this);

  ui->gridLayout->addWidget(vehicleDynamics);
  setWindowIcon(QIcon(":/img/img/vehicle_dynamics.png"));

  // reads layout file
  namespace pt = boost::property_tree;

  std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                        "graphical_user_interface/layouts/layout.json");
  pt::read_json(layout_dir, root);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;
  int height = root.get<int>("VEHICLE_DYNAMICS_VIEWER.height");
  int width = root.get<int>("VEHICLE_DYNAMICS_VIEWER.width");

  this->resize(width, height);
  this->move(x0 + root.get<int>("VEHICLE_DYNAMICS_VIEWER.position.x"), y0 + root.get<int>("VEHICLE_DYNAMICS_VIEWER.position.y"));

  // Settings connections
  setUp();
}

VehicleDynamicsViewerView::~VehicleDynamicsViewerView()
{
  delete ui;
  delete vehicleDynamics;
}

void VehicleDynamicsViewerView::setUp()
{
  n.param<std::string>("robot_namespace", robot_namespace, "drone1");
  n.param<std::string>("window_event_topic", window_event_topic, "window_event");


  // Subscribers
  window_event_sub = n.subscribe("/" + robot_namespace + "/" + window_event_topic, 10,
                                   &VehicleDynamicsViewerView::windowOpenCallback, this);

  // Publishers
   window_event_pub =
      n.advertise<aerostack_msgs::WindowEvent>("/" + robot_namespace + "/" + window_event_topic, 1, true);
}

VehicleDynamicsViewer* VehicleDynamicsViewerView::getVehicleDynamicsViewer()
{
  return vehicleDynamics;
}

void VehicleDynamicsViewerView::closeEvent(QCloseEvent* event)
{
  window_event_msg.window = aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER;
  window_event_msg.event = aerostack_msgs::WindowEvent::CLOSE;
  window_event_pub.publish(window_event_msg);
}

void VehicleDynamicsViewerView::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void VehicleDynamicsViewerView::windowOpenCallback(const aerostack_msgs::WindowEvent& msg)
{
  window_event_msg = msg;
  if (window_event_msg.window == aerostack_msgs::WindowEvent::PYTHON_MISSION_INTERPRETER ||
      window_event_msg.window == aerostack_msgs::WindowEvent::ALPHANUMERIC_INTERFACE_CONTROL)
  {
    window_event_msg.window = aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER;
    window_event_pub.publish(window_event_msg);

    killMe();
  }
  if (window_event_msg.window == aerostack_msgs::WindowEvent::INTEGRATED_VIEWER && window_event_msg.event == aerostack_msgs::WindowEvent::MINIMIZE)
  {
    showMinimized();
  }
}
