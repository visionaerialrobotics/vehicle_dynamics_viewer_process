/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/vehicle_dynamics_viewer.h"
#include <iostream>

using namespace Eigen;
VehicleDynamicsViewer::VehicleDynamicsViewer(QWidget* parent) : QWidget(parent), ui(new Ui::VehicleDynamicsViewer)
{
  ui->setupUi(this);
  ros::start();
  ros::NodeHandle n;

  odometryReceiver = new OdometryStateReceiver();

  if (ros::this_node::getNamespace().compare("/") == 0)
    rosnamespace.append("/drone1");  // default namespace
  else
    rosnamespace.append(ros::this_node::getNamespace());
  QObject::connect(odometryReceiver, SIGNAL(updateStatus()), this, SLOT(updateDynamicsPanel()));
  odometryReceiver->openSubscriptions(n, rosnamespace);
}

VehicleDynamicsViewer::~VehicleDynamicsViewer()
{
  delete ui;
}
void VehicleDynamicsViewer::updateDynamicsPanel()
{
  // Vehicle position
  QString value_x = QString::number(odometryReceiver->odometry_new_pose.pose.position.x, 'f', 2);
  QString value_y = QString::number(odometryReceiver->odometry_new_pose.pose.position.y, 'f', 2);
  QString value_z = QString::number(odometryReceiver->odometry_new_pose.pose.position.z, 'f', 2);

  Eigen::Quaternionf q(
      odometryReceiver->odometry_new_pose.pose.orientation.w, odometryReceiver->odometry_new_pose.pose.orientation.x,
      odometryReceiver->odometry_new_pose.pose.orientation.y, odometryReceiver->odometry_new_pose.pose.orientation.z);

  float yaw, pitch, roll;

  toEulerAngle(q, roll, pitch, yaw);

  QString value_yaw = QString::number(angles::to_degrees(yaw), 'f', 2);
  QString value_pitch = QString::number(angles::to_degrees(pitch), 'f', 2);
  QString value_roll = QString::number(angles::to_degrees(roll), 'f', 2);

  if (value_x == "-0.00")
    ui->value_vehicle_x->setText("0.00");  // This prevents displaying -0.00
  else
    ui->value_vehicle_x->setText(value_x);
  if (value_y == "-0.00")
    ui->value_vehicle_y->setText("0.00");  // This prevents displaying -0.00
  else
    ui->value_vehicle_y->setText(value_y);
  if (value_z == "-0.00")
    ui->value_vehicle_z->setText("0.00");  // This prevents displaying -0.00
  else
    ui->value_vehicle_z->setText(value_z);
  if (value_yaw == "-0.00")
    ui->value_vehicle_yaw->setText("0.00");  // This prevents displaying -0.00
  else
    ui->value_vehicle_yaw->setText(value_yaw);
  if (value_pitch == "-0.00")
    ui->value_vehicle_pitch->setText("0.00");  // This prevents displaying -0.00
  else
    ui->value_vehicle_pitch->setText(value_pitch);
  if (value_roll == "-0.00")
    ui->value_vehicle_roll->setText("0.00");  // This prevents displaying -0.00
  else
    ui->value_vehicle_roll->setText(value_roll);
}

void VehicleDynamicsViewer::toEulerAngle(const Eigen::Quaternionf& q, float& roll, float& pitch, float& yaw)
{
  // roll (x-axis rotation)
  double sinr_cosp = +2.0 * (q.w() * q.x() + q.y() * q.z());
  double cosr_cosp = +1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y());
  roll = atan2(sinr_cosp, cosr_cosp);

  // pitch (y-axis rotation)
  double sinp = +2.0 * (q.w() * q.y() - q.z() * q.x());
  if (fabs(sinp) >= 1)
    pitch = copysign(M_PI / 2, sinp);  // use 90 degrees if out of range
  else
    pitch = asin(sinp);

  // yaw (z-axis rotation)
  double siny_cosp = +2.0 * (q.w() * q.z() + q.x() * q.y());
  double cosy_cosp = +1.0 - 2.0 * (q.y() * q.y() + q.z() * q.z());
  yaw = atan2(siny_cosp, cosy_cosp);
}
